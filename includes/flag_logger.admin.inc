<?php

/**
 * @file
 * Contains administrative pages for configuring flag logging.
 */

/**
 * A form for configuring flag logging.
 */
function flag_logging_configure(&$form_state) {

  $flags = flag_get_flags();
  $form = array();
  // turn on watchdog reporting for flags
  $form['watchdog_reporting'] = array(
    '#type' => 'checkbox',
    '#title' => t('Report all flagging to Watchdog.'),
    '#default_value' => variable_get('flag_logger_watchdog_reporter', 0),
  );

  // Turn on logging for global flags only
  $form['global_flags_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only log global flags.'),
    '#default_value' => variable_get('flag_logger_global_flag_logging_only', 0),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit form function for flag_logging_configure.
 */
function flag_logging_configure_submit($form, &$form_state) {
  // Save flag_logger_watchdog_reporter watchdog flag logging option.
  if ($form_state['values']['watchdog_reporting'] == '1') {
    variable_set('flag_logger_watchdog_reporter', 1);
  }
  else {
    variable_set('flag_logger_watchdog_reporter', '0');
  }

  if ($form_state['values']['global_flags_only'] == '1') {
    variable_set('flag_logger_global_flag_logging_only', 1);
  }
  else {
    variable_set('flag_logger_global_flag_logging_only', 0);
  }

  drupal_set_message(t('Flag logging options have been configured.'));
}
